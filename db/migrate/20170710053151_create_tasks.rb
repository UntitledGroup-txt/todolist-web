class CreateTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :tasks do |t|
      t.references :task_list, foreign_key: true
      t.string :title, null: true, default: 'Untitled Task'
      t.string :description, null: true
      t.datetime :due_date, null: true
      t.boolean :notify, null: false, default: true
      t.boolean :checked, null: false, default: false
      t.boolean :notified, null: false, default: false
      t.timestamps
    end
  end
end
