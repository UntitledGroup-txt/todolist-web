class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :username, null: false
      t.string :name, null: false
      t.string :password, null: false
      t.string :surname, null: false
      t.string :email, null: false
      t.column :last_used_list_id, :integer, null: true
      t.timestamps
    end
  end
end
