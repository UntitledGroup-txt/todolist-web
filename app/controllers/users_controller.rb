class UsersController < ApplicationController
  skip_before_action :verify_authenticity_token

  def login
    @user = User.find_by(username: params[:username], password: params[:password])
    if @user
      if request.get?
        render json: {status: 'success', user_id: @user.id}
      end

      if request.post?
        respond_to do |format|
          require 'json'
          format.json {render json: {status: 'success', user_id: @user.id}}
        end
      end
    else
      if request.get?
        render json: {status: 'error'}
      end

      if request.post?
        respond_to do |format|
          require 'json'
          format.json {render json: {status: 'error'}}
        end
      end
    end
  end

  def welcome
    # Do nothing
  end

  def registerpage
    render 'register'
  end

  def register
    if (params[:username].blank? || params[:password].blank?) ||
        (params[:againpassword].blank? || params[:name].blank?) ||
        (params[:surname].blank? || params[:email].blank?)
      return render json: {status: 'error', msg: 'All fields are required'}
    end

    if User.exists?(username: params[:username])
      return render json: {status: 'error', msg: 'This username is already taken'}
    end

    if params[:password]!=params[:againpassword]
      return render json: {status: 'error', msg: 'Passwords does not match'}
    end

    @user = User.new(username: params[:username],
                     password: params[:password],
                     email: params[:email],
                     name: params[:name],
                     surname: params[:surname])

    if(@user.save)
      return render json: {status: 'success', userid: @user.id}
    else
      return render json: {status: 'error'}
    end
  end

end
