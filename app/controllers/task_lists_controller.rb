class TaskListsController < ApplicationController
  #before_action :set_task_list, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token

  def fetchLists
    @lists = TaskList.where(user_id: params[:task_user_id])
    @user = User.find_by(id: params[:task_user_id])
    return render json: {status: 'success', lists: @lists,
                         'lastusedList': @user.last_used_list_id}
  end

  def addList
    if params[:list_name].blank?
      return render json: {status: 'error'}
    end

    @task_list = TaskList.new(user_id: params[:task_user_id],
                              title: params[:list_name])
    if @task_list.save
      @user = User.find_by_id(params[:task_user_id])
      @user.update_attribute(:last_used_list_id, @task_list.id)
      return render json: {status: 'success'}
    end

    return render json: {status: 'error'}
  end

end
