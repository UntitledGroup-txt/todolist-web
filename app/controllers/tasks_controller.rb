# Controller class for Tasks
class TasksController < ApplicationController
  # Skip CSRF checks [Temporary]
  skip_before_action :verify_authenticity_token

  def edit_task
    @task = Task.find_by_id(params[:task_id])

    if request.post?
      # If updating process is successful
      if @task.update_attribute(:description, params[:task_detail]) and
          @task.update_attribute(:notify, params[:task_notify]) and
          @task.update_attribute(:title, params[:task_title]) and
          @task.update_attribute(:due_date, params[:task_date])
        respond_to do |format|
          format.json {head :no_content}
        end
      else
        respond_to do |format|
          format.json {render status: 400}
        end
      end
    end
  end

  # Handle "fetch tasks" POST/GET requests from client, holds READ in CRUD:
  def push_tasks
    # Find all tasks owned by task list sent by JSON object
    @tasks = Task.where(task_list_id: params[:task_list_id]).all

    # If received request is a GET request (ANDROID INTERFACE)
    if request.get?
      render json: @tasks.to_json
      return
    end

    # If received request is a POST request (WEB INTERFACE)
    if request.post?
      #If a user ID is present, set last used list
      if params[:user_id]
        @user = User.find_by_id(params[:user_id])
        @user.update_attribute(:last_used_list_id, params[:task_list_id])
      end

      # Send all related tasks as an JSON object
      respond_to do |format|
        require 'json'
        format.json {render json: @tasks.to_json}
      end
      return
    end
  end

  # Handle GET requests to index page from client:
  def index
    # Do nothing except rendering the index page
  end

  # Handle "delete task" POST/GET requests from client, holds DELETE in CRUD:
  def delete_task
    # Find the task to be removed whose id is sent in JSON object
    @task = Task.find_by_id(params[:task_id])

    # If received request is a GET request
    if request.get?
      if @task.destroy # If deleting process is successful
        render json: {status: 'success'}
      else # If deleting process is not successful
        render json: {status: 'error'}
      end
      return
    end

    # If received request is a POST request
    if request.post?
      if @task.destroy # If deleting process is successful
        # Send a dummy json object to denote process was successful
        respond_to do |format|
          format.json {head :no_content}
        end
      else # If deleting process is not successful
        # Send an error object to denote process was not successful
        respond_to do |format|
          format.json {render status: 400}
        end
      end
    end
  end

  # Handle "update check status of task" POST requests from client, holds UPDATE in CRUD:
  def check_task
    @task = Task.find_by_id(params[:task_id])

    if request.get?
      if @task.update_attribute(:checked, params[:checked]) # If updating process is successful
        render json: {status: 'success'}
      else # If updating process is not successful
        render json: {status: 'error'}
      end
    end

    # If received request is a POST request
    if request.post?
      if @task.update_attribute(:checked, params[:checked]) # If updating process is successful
        # Send a dummy json object to denote process was successful
        respond_to do |format|
          format.json {head :no_content}
        end
      else # If updating process is not successful
        # Send an error object to denote process was not successful
        respond_to do |format|
          format.json {render status: 400}
        end
      end
    end
  end

  # Handle post request to tasks/addTask for adding new task
  def addTask
    # if task has a title
    if (!params[:task_title].blank?)
      @task = Task.new(:title => params[:task_title],
                       :task_list_id => params[:task_list],
                       :description => params[:task_detail],
                       :due_date => params[:task_due_date],
                       :notify => params[:task_notify],
                       :checked => params[:task_check])
      # if task does not have a title
    else
      @task = Task.new(:title => "Untitled Task",
                       :task_list_id => params[:task_list],
                       :description => params[:task_detail],
                       :due_date => params[:task_due_date],
                       :notify => params[:task_notify],
                       :checked => params[:task_check])
    end

    if @task.save # If adding is successful
      return render json: {status: 'success'}
    else # If adding is not successful
      return render json: {status: 'error'}
    end
  end
end

