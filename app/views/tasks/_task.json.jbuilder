json.extract! task, :id, :task_list_id, :title, :description, :due_date, :notify, :created_at, :updated_at
json.url task_url(task, format: :json)
