class NotificationCheckJob < ApplicationJob
  queue_as :default

  def perform(*args)
    @tasks = Task.where(notified: false, notify: true).all

    @tasks.each do |t|
      if ((t.due_date - DateTime.now) / 1.hours) <= 1
        require 'fcm'
        fcm = FCM.new("AAAAgrDZRn8:APA91bEtZOGpdBU8qYX9gUtMwAetoqjXOEhqOO5nMGI4UIKiiTWUIX88j2KjoH4IRr5Snr7LJNIBszIT-aJjwnPuw_gBi3dE1CJTHg1XLCQJUFKUgNJWaxyU345PGWRt4Jbjo5VTwMg4")
        response = fcm.send_to_topic(t.task_list.user.username,
                                     notification: {body: t.description, title: t.title})
        puts response
        t.notified=true
        t.save
      end
    end

    NotificationCheckJob.set(wait: 30.second).perform_later
  end
end
