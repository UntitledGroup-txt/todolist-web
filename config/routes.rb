Rails.application.routes.draw do
  # Admin interface for managing database:
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  # Redirect root address to index in tasks controller:
  root to: 'users#welcome'

  post 'tasks/editTask' => 'tasks#edit_task'

  # Handle fetch tasks POST/GET requests from client:
  post 'tasks/fetchTasks' => 'tasks#push_tasks'
  get '/tasks/fetchTasks/task_list_id=:task_list_id' => 'tasks#push_tasks'

  # Handle fetch task lists POST requests from client:
  post 'tasks/fetchLists' => 'tasks#push_lists'

  # Handle delete task POST/GET requests from client:
  post 'tasks/deleteTask' => 'tasks#delete_task'
  get '/tasks/deleteTask/task_id=:task_id' => 'tasks#delete_task'

  # Handle check task POST/GET requests from client:
  post 'tasks/checkTask' => 'tasks#check_task'
  get '/tasks/checkTask/task_id=:task_id(/checked=:checked)' => 'tasks#check_task'

  # Handle POST request for adding task:
  post 'tasks/addTask' => 'tasks#addTask'

  get 'welcome' => 'users#welcome', as: 'welcome'

  get 'login/username=:username(/password=:password)' => 'users#login'
  post 'login' => 'users#login'

  get 'register' => 'users#registerpage'
  post 'users/register' => 'users#register'

  get 'index' => 'tasks#index'
  post 'index' => 'tasks#index'

  post 'task_lists/addList' => 'task_lists#addList'
  post 'task_lists/fetchLists' => 'task_lists#fetchLists'


end
